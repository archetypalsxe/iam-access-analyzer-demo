# IAM Access Analyzer Demo
Example CloudFormation template(s) for AWS's IAM Access Analyzer

## Running:
* `aws cloudformation deploy --template-file simple.yaml --stack-name SimpleAccessAnalyzer`
* `aws cloudformation deploy --template-file archive-rule.yaml --stack-name ArchiveRuleAccessAnalyzer`
